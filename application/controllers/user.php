<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_user');
    }

    function index()
    {
        $cek = $this->m_user->get_data_art();
        if ($cek > 0) {
            $data['data'] = $cek;
        }

        $data['content'] = "userList";
        $this->load->view('main', $data);
    }

    function customer()
    {
        $cek = $this->m_user->get_data_cust();
        if ($cek > 0) {
            $data['data'] = $cek;
        }

        $data['content'] = "customerList";
        $this->load->view('main', $data);
    }

    function user_add()
    {
        $role = $this->m_user->get_role();
        if ($role > 0) {
            $data['data_role'] = $role;
        }

        $job = $this->m_user->get_job();
        if ($job > 0) {
            $data['data_job'] = $job;
        }

        $data['content'] = "userAdd";
        $this->load->view('main', $data);
    }

    function action_add()
    {
        $uname = $this->input->post('uname');
        $pass = $this->input->post('pass');
        $email = $this->input->post('email');
        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $nik = $this->input->post('nik');
        $role = $this->input->post('role');
        $job = $this->input->post('job');
        $price = $this->input->post('price');
        $bank = $this->input->post('bank');
        $accountnumb = $this->input->post('accountnumb');

        $data = array(
            'UserName' => $uname,
            'Password' => $pass,
            'EmailAddress' => $email,
            'Name' => $name,
            'PhoneNumber' => $phone,
            'NIK' => $nik,
            'RoleId' => $role,
            'JobsId' => $job,
            'Price' => $price,
            'BankAccountNumber' => $accountnumb,
            'Bank' => $bank
        );

        $this->m_user->input_data($data, 'msuser');
        redirect(base_url('user'));
    }

    function user_edit()
    {
        $id = $this->input->get('id');
        $user = $this->m_user->get_data_byid($id);
        if ($user > 0) {
            $data['data_user'] = $user;
        }

        $role = $this->m_user->get_role();
        if ($role > 0) {
            $data['data_role'] = $role;
        }

        $job = $this->m_user->get_job();
        if ($job > 0) {
            $data['data_job'] = $job;
        }

        $data['content'] = "userEdit";
        $this->load->view('main', $data);
    }

    function action_edit()
    {
        $id = $this->input->get('id');
        $uname = $this->input->post('uname');
        $pass = $this->input->post('pass');
        $email = $this->input->post('email');
        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $nik = $this->input->post('nik');
        $role = $this->input->post('role');
        $job = $this->input->post('job');
        $price = $this->input->post('price');
        $bank = $this->input->post('bank');
        $accountnumb = $this->input->post('accountnumb');

        $data = array(
            'UserName' => $uname,
            'Password' => $pass,
            'EmailAddress' => $email,
            'Name' => $name,
            'PhoneNumber' => $phone,
            'NIK' => $nik,
            'RoleId' => $role,
            'JobsId' => $job,
            'Price' => $price,
            'BankAccountNumber' => $accountnumb,
            'Bank' => $bank
        );

        $where = array(
            'id' => $id
        );

        $this->m_user->update_data($where,$data,'msuser');
        redirect(base_url('user'));
    }

    function action_delete()
    {
        $id = $this->input->post('id', TRUE);
        $where = array('id' => $id);

        $this->m_user->delete_data($where,'msuser');
        redirect(base_url('user'));
    }

}
