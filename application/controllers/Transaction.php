<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();		
		$this->load->model('m_transaction');
	}

	public function index()
	{
		
		// $where = array(
		// 	'RoleId' => 3 
		// );
		$data['data'] = $this->m_transaction->getTrans();
		// $dataTypeART = $this->m_transaction->getDataTypeART();
		$data['content']="transactionList";
		// $data['dataART']=$dataART;
		// $data['dataTypeART']=$dataTypeART;
		// print_r($dataART); die();
		// return $data;
        // $this->load->view('admin/mainlogin',$data);
		$this->load->view('main',$data);
	}
	
	public function payment()
	{
        $data['content']="payment";
        // $this->load->view('admin/mainlogin',$data);
		$this->load->view('main',$data);
	}
	public function choosePayment()
	{
        $data['content']="choosepayment";
        // $this->load->view('admin/mainlogin',$data);
		$this->load->view('main',$data);
	}

	public function getART()
	{
		$order = $this->input->post('OrderBy');
		$category = $this->input->post('gridRadios');
		$price = $this->input->post('PriceRange');

		// echo  $order ."-".$category."-".$price; die();	
			// $where = array(
		// 	'RoleId' => 3 
		// );
		$dataTypeART = $this->m_transaction->getDataTypeART();
		$dataART = $this->m_transaction->getListARTFilter($order,$category,$price);
		// print_r($dataART); die();

		$data['content']="transaction";
		$data['dataART']=$dataART;
		$data['dataTypeART']=$dataTypeART;
		$this->load->view('main',$data);
	}

	public function orderART()
	{
		$idART = $this->input->post('ArtId');
		$idUser = $this->session->userdata("id");
		
		$dataCust = $this->m_transaction->getUserById($idUser);
		$data['dataCustomer'] = $dataCust;
		$dataART = $this->m_transaction->getUserById($idART);
		// print_r($dataART);die();
		$data['dataART'] = $dataART;
		$data['content']="payment";
		$this->load->view('main',$data);
	}
	public function orderNow()
	{
		$dateTrans = date('yy-m-d');
		$dateStart = $this->input->post('dateStart');
		$dateStarts = DateTime::createFromFormat('Y-m-d', $dateStart);
		date_add($dateStarts,date_interval_create_from_date_string("30 days"));
		$dateEnd=date_format($dateStarts,"yy-m-d");

		// echo $dateEnd;
		// die();

		$param = array(
			'Id_Customer' => $this->input->post('idCustomer'),
			'Id_ART' => $this->input->post('idART'),
			'Price' => $this->input->post('priceTotal'),
			'Form_Persetujuan' => true,
			'Transaction_Date' => $dateTrans,
			'Start_Date' => $dateStart,
			'End_Date' => $dateEnd,
			'StatusPay' => 1
		);

		$idTrans = $this->m_transaction->insertTrans($param);
		$data['idTransaction'] = $idTrans;
		$dataTrans = $this->m_transaction->getDataTrans($idTrans);
		$data['dataTransaction'] = $dataTrans;
		$idUser = $dataTrans[0]['Id_Customer'];
		$idART = $dataTrans[0]['Id_ART'];
		$dataCust = $this->m_transaction->getUserById($idUser);
		$data['dataCustomer'] = $dataCust;
		$dataART = $this->m_transaction->getUserById($idART);
		$data['dataART'] = $dataART;
		$data['content']="choosePayment";
		$this->load->view('main',$data);
	}

	public function getDataBank(){
		$dataART = $this->m_transaction->getDataBank();
		$data['content']="transaction";
		$data['dataART']=$dataART;
	}

	public function payNow(){
		try{
			$idTrans = $this->input->post('idTrans');
			$idBank = $this->input->post('gridRadios');
			$dataTrans = $this->m_transaction->getDataTrans($idTrans);
			$data['dataTransaction'] = $dataTrans;

			$where = array(
				'Id' => $idTrans
			);
			$param = array(
				'Id_AccountNumComp' => $idBank
			);

			$dataART = $this->m_transaction->updateTrans($where,$param);
			$data['success']=true;
			$data['content']="dashboard";
			$this->load->view('main',$data);
			return true;
		}
		catch(Exception $e){
			return false;
		}
	}

}