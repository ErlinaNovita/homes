<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Job extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */

    function __construct()
    {
        parent::__construct();
        $this->load->model('m_job');
    }

    function index()
    {
        $cek = $this->m_job->get_data();
        if ($cek > 0) {
            $data['data'] = $cek;
        }

        $data['content'] = "jobList";
        $this->load->view('main', $data);
    }

    function action_add()
    {
        $job = $this->input->post('job');

        $data = array(
            'Jobs_TypeName' => $job
        );

        $this->m_job->input_data($data, 'msjobs_type');
        redirect(base_url('job'));
    }

    function action_edit()
    {
        $id = $this->input->get('id');
        $uname = $this->input->post('uname');
        $pass = $this->input->post('pass');
        $email = $this->input->post('email');
        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $nik = $this->input->post('nik');
        $role = $this->input->post('role');
        $job = $this->input->post('job');
        $price = $this->input->post('price');
        $bank = $this->input->post('bank');
        $accountnumb = $this->input->post('accountnumb');

        $data = array(
            'UserName' => $uname,
            'Password' => $pass,
            'EmailAddress' => $email,
            'Name' => $name,
            'PhoneNumber' => $phone,
            'NIK' => $nik,
            'RoleId' => $role,
            'JobsId' => $job,
            'Price' => $price,
            'BankAccountNumber' => $accountnumb,
            'Bank' => $bank
        );

        $where = array(
            'id' => $id
        );

        $this->m_user->update_data($where, $data, 'msuser');
        redirect(base_url('user'));
    }

    function action_delete()
    {
        $id = $this->input->post('id', TRUE);

        print_r($id);
        die();
        $where = array('id' => $id);

        $this->m_job->delete_data($where, 'msjobs_type');
        redirect(base_url('job'));
    }
}
