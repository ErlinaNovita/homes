<?php

// use ___PHPSTORM_HELPERS\object;

defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_login');
	}

	function index()
	{
		$data['info'] = array(
			array(
				'logo' => 'assets/images/ic_sertificate.png',
				'title' => 'Asisten Bersertifikat',
				'msg' => 'Memudahkan pekerjaan rumah Anda.'
			),
			array(
				'logo' => 'assets/images/ic_pay.png',
				'title' => 'Kemudahan Pembayaran',
				'msg' => 'Fitur pembayaran online kapan saja dan dimana saja.'
			),
			array(
				'logo' => 'assets/images/ic_money.png',
				'title' => 'Homes Point',
				'msg' => 'Dapatkan 	poin setiap transaksinya dan tukarkan dengan voucher.'
			)
		);

		// print_r($data['info']);
		// die();
        $data['content'] = "login";
		$this->load->view('mainlogin', $data);
	}

	function login()
	{
		$username = $this->input->post('username');
		$pass = $this->input->post('password');

		$cek = $this->m_login->get_data($username, $pass);
		if ($cek > 0) {
			$data_session = array(
				'nama' => $username,
				'status' => "login",
				'role' => $cek[0]['RoleId'],
				'id' => $cek[0]['Id'],
				'phone' => $cek[0]['PhoneNumber'],
				'address' => $cek[0]['Address'],
			);

			$this->session->set_userdata($data_session);

			redirect(base_url("dashboard"));
			// echo $cek[0]['RoleId'];
		} else {
			echo "Username dan password salah !";
		}
	}

	function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('welcome'));
	}
}
