<?php
class M_user extends CI_Model{
    function get_data_art(){		
        $sql = "select *, msuser.Id as UserId from msuser
        left join msjobs_type on msuser.JobsId = msjobs_type.Id
        left join ms_role on msuser.RoleId = ms_role.Id where msuser.RoleId = 3";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_data_cust(){		
      $sql = "select *, msuser.Id as UserId from msuser
      left join msjobs_type on msuser.JobsId = msjobs_type.Id
      left join ms_role on msuser.RoleId = ms_role.Id where msuser.RoleId = 2";
      $query = $this->db->query($sql);
      return $query->result_array();
  }

    function get_data_byid($id){		
        $sql = "select *, msuser.Id as UserId from msuser
        left join msjobs_type on msuser.JobsId = msjobs_type.Id
        left join ms_role on msuser.RoleId = ms_role.Id
        where msuser.Id =".$id;
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_role(){		
        $sql = "select * from ms_role";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function get_job(){		
        $sql = "select * from msjobs_type";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    function input_data($data,$table){
		$this->db->insert($table,$data);
    }
    
    function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
    }	
    
    function delete_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
}
