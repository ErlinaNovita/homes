<?php
class M_Transaction extends CI_Model{	
	function getListART(){	
                $sql = "select u.Id AS UserId,u.Name,u.Address,u.Price,u.PhoneNumber,j.Jobs_TypeName from msuser u
                        LEFT JOIN msjobs_type j ON u.JobsId = j.Id
                        where RoleId=3";
                $query = $this->db->query($sql);
                return $query->result_array();	
                        // return $this->db->get_where("msuser",$where);
        }	
        function getListARTFilter($order,$type,$price){
                $sql = "select u.Id AS UserId,u.Name,u.Address,u.Price,u.PhoneNumber,j.Jobs_TypeName from msuser u
                        LEFT JOIN msjobs_type j ON u.JobsId = j.Id
                        where RoleId=3 AND ((u.JobsId='".$type."' AND '".$type."'!=0)
                        ||(u.JobsId !=0 AND '".$type."'=0)) AND ((u.price < 3000000 AND '".$price."'=1)
                        ||(u.price >= 3000000 AND u.price <= 5000000 AND '".$price."'=2)
                        ||(u.price > 5000000 AND '".$price."'=3)) 
                        ORDER BY u.price ".$order."";
                $query = $this->db->query($sql);
                return $query->result_array();	
                        // return $this->db->get_where("msuser",$where);
        }
        function getUserById($id){	
                $sql = "select u.Id AS UserId,u.Name,u.Address,u.Price,u.PhoneNumber,j.Jobs_TypeName,u.EmailAddress 
                        from msuser u 
                        LEFT JOIN msjobs_type j ON u.JobsId = j.Id
                        where u.Id='".$id."'";
                $query = $this->db->query($sql);
                return $query->result_array();	
                        // return $this->db->get_where("msuser",$where);
        }
        function insertTrans($param){
            $this->db->insert('trservicesrent', $param);
            return $this->db->insert_id();
        }
        function getDataTrans($id){
                $sql = "select * from trservicesrent where Id='".$id."'";
                $query = $this->db->query($sql);
                return $query->result_array();	
        }
        function getTrans(){
                $sql = "select t.* , m.Name as customer_name , ms.Name as art_name from trservicesrent t inner join msuser m on t.Id_Customer = m.Id
                        inner join msuser ms on t.Id_ART = ms.Id";
                $query = $this->db->query($sql);
                return $query->result_array();	
        }
        function updateTrans($where,$data){
                $this->db->where($where);
		$this->db->update("trservicesrent",$data);
        }
        function getDataTypeART(){
                $sql = "select * from msjobs_type";
                $query = $this->db->query($sql);
                return $query->result_array();	
        }
}
?>