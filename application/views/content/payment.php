<form action="<?php echo base_url('transaction/orderNow');?>" method="post" id="OrderNow">
    <div class="col-md-6">
        <div class="panel panel-success" style="margin-bottom:20px">
            <div class="panel-heading">
                <h3 class="panel-title">Info Pemesan</h3>
            </div>
            <div class="panel-body"> 
                <h4><?php echo $this->session->userdata('nama');?></h4>
                <br>
                <h5><?php echo $this->session->userdata('phone');?></h5>
                <br>
                <h6><?php echo $this->session->userdata('address');?></h6>
            </div>
        </div>
        <div class="panel panel-success" style="margin-bottom:20px">
            <div class="panel-heading">
                <h3 class="panel-title">Detail Harga</h3>
            </div>
            <div class="panel-body"> 
                <h4> <?php echo $dataART[0]['Name'];?> ( <?php echo $dataART[0]['Jobs_TypeName'];?> )</h4>
                <br>
                <!-- <div class="row"> -->
                <div class="col-md-10">
                    <div class="panel panel-success">
                        <div class="panel-body"> 
                            <div class="col-md-6">
                                <h5><b>Jasa ART</b></h5>
                                <h5><b>Asuransi</b></h5>
                                <h5><b>Biaya Admin</b></h5>
                            </div>
                            <div class="col-md-6 pull-right">
                                <h5><?php echo $dataART[0]['Price'];?></h5>
                                <h5>100.000</h5>
                                <h5>10.000</h5>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="panel-footer"> 
                        <h4>Total : Rp <?php echo $dataART[0]['Price']+100.000+10.000;?></h4>
                    </div>
                </div>
                <!-- </div> -->
                
            </div>
        </div>
        <div class="panel panel-success" style="margin-bottom:20px">
            <div class="panel-body"> 
                <h5>Mohon untuk membaca syarat dan ketentuan yang berlaku,klik tombol dibawah ini<h5>
                <br>
                <button type="button" class="btn btn-primary" style="background:#339966" data-toggle="modal" data-target="#acceptModal"><b>S&K</b></button>
                <br>
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="Accept" name="Accept">
                    <label class="custom-control-label" for="customCheck1">Setuju</label>
                </div>
                <h6>*Anda harus menyetujui dokumen tersebut untuk melanjutkan</h6>
            </div>
        </div>
    </div>
    <div class="col-md-6">
    <div class="panel panel-success" style="margin-bottom:20px">
            <div class="panel-heading">
                <h3 class="panel-title">Tanggal Mulai Pekerjaan</h3>
            </div>
            <div class="panel-body"> 
                <div class="row">
                    <div class="col-md-9">
                        <input type="date" class="form-control" name="dateStart" id="dateStart">
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-success" style="margin-bottom:20px">
            <div class="panel-heading">
                <h3 class="panel-title">Detail Pemesanan</h3>
            </div>
            <div class="panel-body"> 
            <div class="row">
                <div class="col-md-3">
                    <img src="<?php echo base_url()?>assets/images/userphoto.png" style="width:60px;height:60px">
                </div>
                <div class="col-md-9">
                    <div class="col-md-7">
                        <h4><?php echo $dataART[0]['Jobs_TypeName'];?></h4>
                        <span><?php echo $dataART[0]['EmailAddress'];?></span>
                        <span><?php echo $dataART[0]['PhoneNumber'];?></span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                ---------------------------------------------------------
                <div class="panel panel-success">
                    <div class="panel-body"> 
                        <div class="col-md-6">
                            <h5><b>Jasa ART  :</b></h5>
                            <h5><b>Asuransi  :</b></h5>
                            <h5><b>Biaya Admin :</b></h5>
                            <h5 id="lblDisc" hidden><b>Discount :</b></h5>
                        </div>
                        <div class="col-md-6" style="padding-left:100px">
                            <h5><?php echo $dataART[0]['Price'];?></h5>
                            <h5>100.000</h5>
                            <h5>10.000</h5>
                            <h5 id="valDisc" hidden>-20.000</h5>
                        </div>
                    </div>
                    <div class="panel-footer"> 
                        <h4 id="totalDisc" hidden>Total : Rp <?php echo $dataART[0]['Price']+100.000+10.000-20.000;?></h4>
                        <h4 id="total">Total : Rp <?php echo $dataART[0]['Price']+100.000+10.000;?></h4>
                    </div>
                </div>
            </div>
            </div>
            <div class="panel-footer"> 
                <h5>Anda akan mendapatkan 100 poin untuk pemesanan ini</h5>
            </div>
        </div>
        <div class="panel panel-success" style="margin-bottom:30px">
            <div class="panel-body"> 
                <div class="col-md-8">
                    <input type="text" id="VoucherCode" class="form-control" placeholder="Masukkan Kode Voucher">
                </div>
                <div class="col-md-4">
                    <button type="button" onclick="discTotal()" class="btn btn-primary" style="width:100px;background:#339966"><b>SUBMIT</b></button>
                </div>
            </div>
        </div>
        
        <button type="button" onclick="saveTrans()" class="btn btn-primary" style="width:522px;background:#339966;height:50px"><b>PESAN SEKARANG</b></button>
    </div>
    <input type="hidden" name="idCustomer" value="<?php echo $dataCustomer[0]['UserId']?>">
    <input type="hidden" name="idART" value="<?php echo $dataART[0]['UserId']?>">
    <input type="hidden" name="priceTotal" id="priceTotal" value="<?php echo $dataART[0]['Price']+100.000+10.000;?>">
</form>
<div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">Syarat & Ketentuan</h3>
      </div>
      <div class="modal-body">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ketentuan Penggunaan ini adalah perjanjian antara pengguna (“Anda”) dan PT Homes Indonesia (“Kami”), 
      sebuah perseroan terbatas yang didirikan dan beroperasi secara sah berdasarkan hukum negara Republik 
      Indonesia dan berdomisili di DKI Jakarta, Indonesia. Ketentuan Penggunaan ini mengatur akses dan 
      penggunaan Anda atas aplikasi,konten dan produk yang disediakan oleh Kami (selanjutnya, secara bersama-sama disebut 
      sebagai “Aplikasi Homes”), serta pemesanan, pembayaran atau penggunaan layanan yang tersedia 
      pada Aplikasi Homes (“Layanan”).
      <br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dengan menyetujui Ketentuan Penggunaan ini, Anda juga menyetujui Ketentuan Penggunaan tambahan, 
      termasuk Ketentuan Penggunaan pada setiap Layanan, dan perubahannya yang merupakan bagian yang tidak 
      terpisahkan dari Ketentuan Penggunaan ini (selanjutnya, Ketentuan Penggunaan, Ketentuan Penggunaan 
      tambahan, dan perubahannya secara bersama-sama disebut sebagai “Ketentuan Penggunaan”). 
      Meskipun merupakan satu kesatuan, Ketentuan Penggunaan tambahan akan berlaku dalam hal 
      terdapat perbedaan dengan Ketentuan Penggunaan.
      <br>
      <b>Kekayaan Intelektual</b>
      <br>
      Aplikasi Homes dan Layanan, termasuk namun tidak terbatas pada, nama, logo, kode program, desain, 
      merek dagang, teknologi, basis data, proses dan model bisnis, dilindungi oleh hak cipta, merek, 
      paten dan hak kekayaan intelektual lainnya yang tersedia berdasarkan hukum Republik Indonesia yang 
      terdaftar baik atas nama Kami ataupun afiliasi Kami. Kami (dan pemberi lisensi Kami) memiliki seluruh 
      hak dan kepentingan atas Aplikasi Homes dan Layanan, termasuk seluruh hak kekayaan intelektual terkait 
      dengan seluruh fitur yang terdapat didalamnya dan hak kekayaan intelektual terkait.
      <br>
      <b>Pemberlakuan Denda</b>
      <br>
      Anda dapat juga dapat terkena denda terkait hal - hal dibawah ini.
      <ul>
        <li>Terbukti melakukan kekerasan terhadap Asisten Rumah Tangga</li>
        <li>Terbukti memberikan beban kerja yang berlebih terhadap Asisten Rumah Tangga</li>
      <ul>
      <b><i>Saya telah membaca dan mengerti seluruh Ketentuan Penggunaan ini dan konsekuensinya dan dengan ini menerima setiap hak, kewajiban, dan ketentuan yang diatur di dalamnya.</i><b>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<div class="clearfix"></div>
<script>
    function discTotal(){
        if($("#VoucherCode").val() !== ''){
            document.getElementById("lblDisc").removeAttribute("hidden");
            document.getElementById("valDisc").removeAttribute("hidden");
            document.getElementById("totalDisc").removeAttribute("hidden");
            $("#total").hide();
            $("#priceTotal").val('<?php echo $dataART[0]['Price']+100.000+10.000-20.000;?>');
        }
        else{
            alert('Anda Belum Memasukkan Kode Voucher');
        }
    }
    function saveTrans(){
        if($("#dateStart").val() == ''){
            alert('Tanggal Mulai Pekerjaan Belum Diisi');
        }
        else if(document.getElementById("Accept").checked == false){
            alert('Syarat dan Ketentuan harus disetujui');
        }
        else{
            document.getElementById("OrderNow").submit();
        }
    }
</script>