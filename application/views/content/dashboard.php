<div class="market-updates">
	<div class="col-md-6 market-update-gd">
		<div class="market-update-block clr-block-1">
			<div class="col-md-8 market-update-left">
				<h3>109</h3>
				<h4>Total Pelanggan</h4>
			</div>
			<div class="col-md-4">
				<!-- <i class="fa fa-user"> </i> -->
				<img src="<?php echo base_url() ?>assets/images/customer.png" style="width:80px;height:80px">
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="col-md-6 market-update-gd">
		<div class="market-update-block clr-block-2">
			<div class="col-md-8 market-update-left">
				<h3>120</h3>
				<h4>Total Asisten Rumah Tangga</h4>
			</div>
			<div class="col-md-4">
				<img src="<?php echo base_url() ?>assets/images/staff.png" style="width:80px;height:80px">
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
	<div class="clearfix"> </div>
</div>
<br><br>
<div class="market-updates">
	<div class="clearfix"> </div>
	<div class="col-md-6 market-update-gd" style="padding-right: 2em;">
		<div class="line-chart">
			<h3>Top Customer</h3>
			<br>
			<ol>
				<li>Cust A</li>
				<li>Cust B</li>
				<li>Cust C</li>
				<li>Cust C</li>
			</ol>
			<br>
			<br>
			<br>
			<h3>Top ART</h3>
			<br>
			<ol>
				<li>Cust A</li>
				<li>Cust B</li>
				<li>Cust C</li>
				<li>Cust C</li>
			</ol>
		</div>
	</div>
	<div class="col-md-6 market-update-gd" style="padding-right: 2em;">
		<div class="line-chart">
			<h3>Transaksi</h3>
			<br>
			<canvas id="line"> </canvas>
			<script>
				var lineChartData = {
					labels: ["Januari", "Februari", "Maret", "April", "Mei"],
					datasets: [{
						fillColor: "rgba(252, 130, 19, 0.74)",
						strokeColor: "#FC8213",
						pointColor: "#FC8213",
						pointStrokeColor: "#fff",
						data: [65, 59, 90, 81, 56]
					}]

				};
				new Chart(document.getElementById("line").getContext("2d")).Line(lineChartData);
			</script>
		</div>
	</div>
</div>

<!--market updates end here-->

<div class="modal" id="successModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Transaksi Sukses !</h5>
			</div>
			<div class="modal-body">
				Selamat Transaksi Anda Berhasil, segera lakukan pembayaran agar kami bisa memproses lebih lanjut.
				Terimakasih ^_^
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
			</div>
		</div>
	</div>
</div>

<!--main page chit chating end here-->

<!--main page chart start here-->
<!--main page chart layer2-->
<div class="chart-layer-2">
	<div class="col-md-6 chart-layer2-left">
	</div>
	<div class="clearfix"> </div>
</div>
<script>
	$(document).ready(function() {
		<?php if (isset($success) && ($success == true)) { ?>
			$('#successModal').modal('show');
		<?php } ?>
	});
</script>