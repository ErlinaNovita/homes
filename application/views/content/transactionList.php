<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Daftar Transaksi</h3>
        </div>
        <div class="panel-body">
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Tanggal Transaksi</th>
                            <th>Pelanggan</th>
                            <th>ART</th>
                            <th>Harga</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($data) != false && empty($data) == false) {
                            $i = 0;
                            foreach ($data as $data[0][]) { ?>
                                <tr>
                                    <td><?= date("d-m-Y", strtotime($data[$i]['Transaction_Date'])); ?></td>
                                    <td><?= $data[$i]['customer_name'] ?></td>
                                    <td><?= $data[$i]['art_name'] ?></td>
                                    <td><?= "Rp ". number_format($data[$i]['Price']) ?></td>
                                    <td><?= date("d-m-Y", strtotime($data[$i]['Start_Date'])); ?></td>
                                    <td><?= date("d-m-Y", strtotime($data[$i]['End_Date'])); ?></td>
                                </tr>
                            <?php $i++;
                            }
                        } else { ?>
                            <tr>
                                <td class="text-center" colspan="6">Belum Ada Transaksi</td>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<div class="chart-layer-2">
	<div class="col-md-6 chart-layer2-left">
	</div>
	<div class="clearfix"> </div>
</div>