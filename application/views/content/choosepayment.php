<form action="<?php echo base_url('transaction/payNow');?>" id="Payment" method="post">
    <div class="col-md-7">
        <div class="panel panel-success" style="margin-right:50px">
            <div class="panel-heading">
                <h3 class="panel-title">PEMBAYARAN</h3>
            </div>
            <div class="panel-body"> 
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gridRadios" value="4" style="width:20px; height:20px;">
                            <label class="form-check-label" for="gridRadios1">
                                <h3 class="panel-title">BANK BCA</h3>
                            </label>
                        </div>
                    </div> 
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?php echo base_url()?>assets/images/LogoBank/bca.jpg" style="width:50px;height:50px">
                            </div>
                            <div class="col-md-9">
                                <h4>8748378775783</h4>
                                <h4>PT Homes Indonesia</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gridRadios"  value="3" style="width:20px; height:20px;">
                            <label class="form-check-label" for="gridRadios1">
                                <h3 class="panel-title">BANK BNI</h3>
                            </label>
                        </div>
                    </div> 
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?php echo base_url()?>assets/images/LogoBank/bni.png" style="width:50px;height:50px">
                            </div>
                            <div class="col-md-9">
                                <h4>7834738754397</h4>
                                <h4>PT Homes Indonesia</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gridRadios"  value="2" style="width:20px; height:20px;">
                            <label class="form-check-label" for="gridRadios1">
                                <h3 class="panel-title">BANK BRI</h3>
                            </label>
                        </div>
                    </div> 
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?php echo base_url()?>assets/images/LogoBank/BRI.png" style="width:50px;height:50px">
                            </div>
                            <div class="col-md-9">
                                <h4>9343882482938</h4>
                                <h4>PT Homes Indonesia</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gridRadios"  value="1" style="width:20px; height:20px;">
                            <label class="form-check-label" for="gridRadios1">
                                <h3 class="panel-title">BANK CIMB NIAGA</h3>
                            </label>
                        </div>
                    </div> 
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?php echo base_url()?>assets/images/LogoBank/cimb.jpg" style="width:50px;height:50px">
                            </div>
                            <div class="col-md-9">
                                <h4>7777939343</h4>
                                <h4>PT Homes Indonesia</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="gridRadios"  value="5" style="width:20px; height:20px;">
                            <label class="form-check-label" for="gridRadios1">
                                <h3 class="panel-title">BANK MANDIRI</h3>
                            </label>
                        </div>
                    </div> 
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3">
                                <img src="<?php echo base_url()?>assets/images/LogoBank/mandiri.jpg" style="width:50px;height:50px">
                            </div>
                            <div class="col-md-9">
                                <h4>2347343470849</h4>
                                <h4>PT Homes Indonesia</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-success" style="margin-bottom:100px">
            <div class="panel-heading">
                <h3 class="panel-title">BOOKING ID</h3>
                <h3 class="panel-title"><?php echo $dataTransaction[0]['Id'].$dataTransaction[0]['Id_Customer'].$dataTransaction[0]['Id_ART'];?></h3>
            </div>
            <div class="panel-body"> 
            <!-- <h4>Pesanan Anda</h4> -->
                <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Pesanan Anda</h3>
                </div>
                    <div class="panel-body">
                        <div class="row">
                            <h5><b>Nama Asisten  :</b> <?php echo $dataART[0]['Name'];?></h5>
                            <h5><b>Jasa  :</b> <?php echo $dataART[0]['Jobs_TypeName'];?></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <input type="hidden" name="idTrans" value="<?php echo $dataTransaction[0]['Id'];?>">
        <button type="button" onclick="payNow()" class="btn btn-primary" style="width:423px;background:#339966;height:50px"><b>BAYAR</b></button>
    </div>
</form>
<div class="clearfix"></div>
<script>
    function payNow(){
        var option=document.getElementsByName('gridRadios');
        if (!(option[0].checked || option[1].checked || option[2].checked|| option[3].checked|| option[4].checked)) 
        {
            alert("Anda belum memilih bank untuk pembayaran");
            // return false;
        }
        else
        {
            document.getElementById("Payment").submit();
        }
    }
</script>