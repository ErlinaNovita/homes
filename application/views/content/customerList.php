<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Daftar Pengguna</h3>
        </div>
        <div class="panel-body">
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Nama</th>
                            <th>Telephone</th>
                            <th>NIK</th>
                            <!-- <th>Peran</th> -->
                            <!-- <th>Pekerjaan</th> -->
                            <!-- <th>Harga</th> -->
                            <!-- <th>Nomer Akun</th> -->
                            <!-- <th>Bank</th> -->
                            <!-- <th>CV</th> -->
                            <!-- <th>Aksi</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($data) != false && empty($data) == false) {
                            $i = 0;
                            foreach ($data as $data[0][]) { ?>
                                <tr>
                                    <td><?= $data[$i]['UserName'] ?></td>
                                    <td><?= $data[$i]['EmailAddress'] ?></td>
                                    <td><?= $data[$i]['Name'] ?></td>
                                    <td><?= $data[$i]['PhoneNumber'] ?></td>
                                    <td><?= $data[$i]['NIK'] ?></td>
                                    <!-- <td><?= $data[$i]['Role_Name'] ?></td> -->
                                    <!-- <td><?= $data[$i]['Jobs_TypeName'] ?></td> -->
                                    <!-- <td><?= $data[$i]['Price'] ?></td> -->
                                    <!-- <td><?= $data[$i]['BankAccountNumber'] ?></td> -->
                                    <!-- <td><?= $data[$i]['Bank'] ?></td> -->
                                    <!-- <td><a href="#">Lihat CV</a></td> -->
                                    <!-- <td>
                                        <div class="btn-group">
                                            <a href="<?= base_url() . 'user/user_edit?id=' . $data[$i]['UserId'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <a class="delete-user" data-toggle="modal" data-target="#modalDelete" data-id="<?= $data[$i]['UserId'] ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </td> -->
                                </tr>
                            <?php $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="5">No result found</td>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<br>
<br>
<br>
<div class="chart-layer-2">
	<div class="col-md-6 chart-layer2-left">
	</div>
	<div class="clearfix"> </div>
</div>