<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">Daftar Asisten Rumah Tangga</h3>
        </div>
        <div class="panel-body">
            <div style="float: right;">
                <a href="<?= base_url() . 'user/user_add' ?>" class="btn btn-success" role="button">Tambah +</a>
            </div>
            <br>
            <br>
            <br>
            <br>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Nama</th>
                            <!-- <th>Telephone</th> -->
                            <!-- <th>NIK</th> -->
                            <!-- <th>Peran</th> -->
                            <th>Pekerjaan</th>
                            <th>Harga</th>
                            <!-- <th>Nomer Akun</th> -->
                            <!-- <th>Bank</th> -->
                            <th>CV</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($data) != false && empty($data) == false) {
                            $i = 0;
                            foreach ($data as $data[0][]) { ?>
                                <tr>
                                    <td><?= $data[$i]['UserName'] ?></td>
                                    <td><?= $data[$i]['EmailAddress'] ?></td>
                                    <td><?= $data[$i]['Name'] ?></td>
                                    <!-- <td><?= $data[$i]['PhoneNumber'] ?></td> -->
                                    <!-- <td><?= $data[$i]['NIK'] ?></td> -->
                                    <!-- <td><?= $data[$i]['Role_Name'] ?></td> -->
                                    <td><?= $data[$i]['Jobs_TypeName'] ?></td>
                                    <td><?= $data[$i]['Price'] ?></td>
                                    <!-- <td><?= $data[$i]['BankAccountNumber'] ?></td> -->
                                    <!-- <td><?= $data[$i]['Bank'] ?></td> -->
                                    <td><a href="#">Lihat CV</a></td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="<?= base_url() . 'user/user_edit?id=' . $data[$i]['UserId'] ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                            <a class="delete-user" data-toggle="modal" data-target="#modalDelete" data-id="<?= $data[$i]['UserId'] ?>"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            <?php $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="5">No result found</td>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Hapus User</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <h4>Apakah Anda yakin menghapus user ini?</h4>
                    <form method="post" action="<?= base_url() . 'user/action_delete' ?>">
                        <input type="hidden" name="id" id="del_id" value="">

                        <div style="margin-top: 5%" class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">OK</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".delete-user").click(function() {
        var id = $(this).attr("data-id");
        $("#del_id").prop("value", id);
    });
</script>