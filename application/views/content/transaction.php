<div class="panel panel-success col-md-4">
    <div class="panel-heading">
        <h3 class="panel-title">FILTER</h3>
    </div>
    <div class="panel-body"> 
        <form action="<?php echo base_url('transaction/getART'); ?>" method="post">
            <div class="form-group">
                <label for="sel1">Urutkan Berdasarkan:</label>
                <select class="form-control" name="OrderBy">
                    <option value="ASC">Harga Terendah</option>
                    <option value="DESC">Harga Tertinggi</option>
                </select>
            </div>
            <div class="form-group">
                <label for="sel1">Kategori:</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="gridRadios"  value="0" checked>
                    <label class="form-check-label" for="gridRadios1">
                        Tampilkan Semua
                    </label>
                </div>
                <?php foreach($dataTypeART as $dataTypeART) { ?>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="gridRadios" value="<?php echo $dataTypeART['Id'];?>">
                        <label class="form-check-label" for="gridRadios2">
                            <?php echo $dataTypeART['Jobs_TypeName'];?>
                        </label>
                    </div>
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="sel1">Rentang Harga:</label>
                <select class="form-control"  name="PriceRange">
                    <option value="1">< 3.000.0000</option>
                    <option value="2">3.000.000 - 5.000.000</option>
                    <option value="3">> 5.000.000</option>
                </select>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="btn btn-success">CARI</button>
            </div>
        </form>
    </div>
</div>
<div class="panel panel-success col-md-8">
    <div class="panel-heading">
        <h3 class="panel-title">LIST ART</h3>
    </div>
    <div class="panel-body"> 
        <?php foreach($dataART as $dataART) { ?>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><?php echo $dataART['Name'];?></h3>
                </div> 
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-3">
                            <img src="<?php echo base_url()?>assets/images/userphoto.png" style="width:100px;height:100px">
                        </div>
                        <form action="<?php echo base_url('transaction/orderART'); ?>" method="post">
                            <div class="col-md-9">
                                <div class="col-md-7">
                                    <span class="fa fa-star" style="color:orange"></span>
                                    <span class="fa fa-star" style="color:orange"></span>
                                    <span class="fa fa-star" style="color:orange"></span>
                                    <span class="fa fa-star"></span>
                                    <span class="fa fa-star"></span>
                                    <h4><?php echo $dataART['Jobs_TypeName'];?></h4>
                                    <span><?php echo $dataART['Price'];?> /bulan</span>
                                    <input type="hidden" name="ArtId" value="<?php echo  $dataART['UserId'];?>">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-3">
                                    <button type="submit" class="btn btn-secondary btn-lg" style="margin-left:93px;height:100px;background:#487485;color:white">PESAN</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


<div class="clearfix"></div>
                                                          
                                                          