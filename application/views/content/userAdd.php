
<h3>Tambah ART</h3>
<br>
<br>
<br>
<form action="<?php echo base_url('user/action_add'); ?>" method="post">
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlInput1">Username</label>
            <input name="uname" class="form-control" placeholder="Username">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlInput1">Email</label>
            <input name="email" class="form-control" placeholder="Email">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlInput1">Nama</label>
            <input name="name" class="form-control" placeholder="Nama">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlInput1">Telepon</label>
            <input name="phone" class="form-control" placeholder="Telepon">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleFormControlInput1">NIK</label>
            <input name="nik" class="form-control" placeholder="NIK">
        </div>
    </div>
    <!-- <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlSelect1">Peran</label>
            <select name="role" class="form-control">
                <?php if (isset($data_role) != false && empty($data_role) == false) {
                    $i = 0;
                    foreach ($data_role as $data[]) { ?>
                        <option value="<?= $data[$i]['Id'] ?>"><?= $data[$i]['Role_Name'] ?></option>
                <?php $i++;
                    }
                } ?>
            </select>
        </div>
    </div> -->
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlSelect1">Pekerjaan</label>
            <select name="job" class="form-control">
                <?php if (isset($data_job) != false && empty($data_job) == false) {
                    $i = 0;
                    foreach ($data_job as $data_job[]) { ?>
                        <option value="<?= $data_job[$i]['Id'] ?>"><?= $data_job[$i]['Jobs_TypeName'] ?></option>
                <?php $i++;
                    }
                } ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlSelect1">CV</label>
            <input name="cv" id="cv" class="form-control" type="file">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="exampleFormControlInput1">Nama Bank</label>
            <input name="bank" class="form-control" placeholder="Bank">
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <label for="exampleFormControlInput1">Nomer Akun Bank</label>
            <input name="accountnumb" class="form-control" placeholder="000000000">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleFormControlInput1">Harga</label>
            <input name="price" class="form-control" placeholder="Rp. 2.000.000">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleFormControlInput1">Password</label>
            <input name="pass" class="form-control" placeholder="*******">
        </div>
    </div>
    <div class="col-md-12" style="margin-left: 24%;margin-top: 4%;">
        <!-- <a type="submit" class="btn btn-info" role="button">Submit</a> -->
        <input type="submit" class="btn btn-success col-md-6" name="Sign In" value="SIMPAN">
    </div>
</form>
<div class="clearfix"> </div>