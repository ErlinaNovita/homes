<h3>Edit ART</h3>
<br>
<br>
<br>
<form action="<?php echo base_url('user/action_edit?id='.$data_user[0]['UserId'].''); ?>" method="post">
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlInput1">Username</label>
            <input name="uname" class="form-control" placeholder="Username" value="<?= $data_user[0]['UserName'] ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlInput1">Email</label>
            <input name="email" class="form-control" placeholder="Email" value="<?= $data_user[0]['EmailAddress'] ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlInput1">Nama</label>
            <input name="name" class="form-control" placeholder="Nama" value="<?= $data_user[0]['Name'] ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlInput1">Telepon</label>
            <input name="phone" class="form-control" placeholder="Telepon" value="<?= $data_user[0]['PhoneNumber'] ?>">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleFormControlInput1">NIK</label>
            <input name="nik" class="form-control" placeholder="NIK" value="<?= $data_user[0]['NIK'] ?>">
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlSelect1">Peran</label>
            <select name="role" class="form-control">
                <?php if (isset($data_role) != false && empty($data_role) == false) {
                    $i = 0;
                    $selected = $data_user[0]['RoleId'];
                    foreach ($data_role as $data[]) {
                        if ($selected == $data[$i]['Id']) {
                            echo '<option value="' . $data[$i]['Id'] . '" selected>' . $data[$i]['Role_Name'] . '</option>';
                        } else {
                            echo '<option value="' . $data[$i]['Id'] . '">' . $data[$i]['Role_Name'] . '</option>';
                        }
                        $i++;
                    }
                } ?>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="exampleFormControlSelect1">Pekerjaan</label>
            <select name="job" class="form-control">
                <?php if (isset($data_job) != false && empty($data_job) == false) {
                    $i = 0;
                    $selectedJob = $data_user[0]['JobsId'];
                    foreach ($data_job as $data_job[]) {
                        if ($selectedJob == $data_job[$i]['Id']) {
                            echo '<option value="' . $data_job[$i]['Id'] . '" selected>' . $data_job[$i]['Jobs_TypeName'] . '</option>';
                        } else {
                            echo '<option value="' . $data_job[$i]['Id'] . '">' . $data_job[$i]['Jobs_TypeName'] . '</option>';
                        }
                        $i++;
                    }
                } ?>
            </select>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="exampleFormControlInput1">Nama Bank</label>
            <input name="bank" class="form-control" placeholder="Bank" value="<?= $data_user[0]['Bank'] ?>">
        </div>
    </div>
    <div class="col-md-8">
        <div class="form-group">
            <label for="exampleFormControlInput1">Nomer Akun Bank</label>
            <input name="accountnumb" class="form-control" placeholder="000000000" value="<?= $data_user[0]['BankAccountNumber'] ?>">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleFormControlInput1">Harga</label>
            <input name="price" class="form-control" placeholder="Rp. 2.000.000" value="<?= $data_user[0]['Price'] ?>">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <label for="exampleFormControlInput1">Password</label>
            <input name="pass" class="form-control" placeholder="*******" value="<?= $data_user[0]['Password'] ?>">
        </div>
    </div>
    <div class="col-md-12" style="margin-left: 24%;margin-top: 4%;">
        <!-- <a type="submit" class="btn btn-info" role="button">Submit</a> -->
        <input type="submit" class="btn btn-success col-md-6" name="Sign In" value="SIMPAN">
    </div>
</form>
<div class="clearfix"> </div>