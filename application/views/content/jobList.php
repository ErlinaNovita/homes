<div>
    <div class="panel panel-success">
        <div class="panel-heading">
            <h3 class="panel-title">LIST JOB</h3>
        </div>
        <div class="panel-body">
            <div>
                <a data-toggle="modal" data-target="#modalAdd" class="btn btn-info" role="button">Add +</a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Jenis Pekerjaan</th>
                            <!-- <th>Aksi</th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($data) != false && empty($data) == false) {
                            $i = 0;
                            foreach ($data as $data[0][]) { ?>
                                <tr>
                                    <td><?= $data[$i]['Jobs_TypeName'] ?></td>
                                    <!-- <td>
                                        <div class="btn-group">
                                            <a class="edit-job" data-toggle="modal" data-target="#modalEdit" data-id="<?= $data[$i]['Id'] ?>"><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>
                                            <a class="delete-job" data-toggle="modal" data-target="#modalDelete" data-id="<?= $data[$i]['Id'] ?>"><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></a>
                                        </div>
                                    </td> -->
                                </tr>
                            <?php $i++;
                            }
                        } else { ?>
                            <tr>
                                <td colspan="5">No result found</td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal Add -->
    <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Input Data Pekerjaan</h3>
                </div>
                <div class="modal-body">
                    <form action="<?php echo base_url('job/action_add'); ?>" method="post">
                        <label for="exampleFormControlInput1">Jenis Pekerjaan</label>
                        <input name="job" class="form-control" placeholder="Pekerjaan">
                        <div style="margin-top: 5%" class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">OK</button>
                        </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Del -->
    <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title">Hapus Pekerjaan</h3>
                </div>
                <div class="modal-body">
                    <h4>Apakah Anda yakin menghapus pekerjaan ini?</h4>
                    <form method="post" action="<?= base_url() . 'job/action_delete' ?>">
                        <input type="hidden" name="id" id="del_id" value="">

                        <div style="margin-top: 5%" class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-primary">OK</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".delete-job").click(function() {
        var id = $(this).attr("data-id");
        $("#del_id").prop("value", id);
    });
</script>