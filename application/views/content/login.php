<body>
    <div>
        <!-- <div id="login-row" style="margin-top: 1%; margin-bottom: -2%">
            <div class="col-xs-1 center-block">
                <h1 style="text-align: center; color: #36ae97">homes</h1>
            </div>
        </div> -->

        <div id="login-row" class="login-page">
            <div class="login-main">
                <?php foreach ($info as $info) { ?>
                    <div class="panel panel-info">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="<?php echo base_url() . $info['logo'] ?>" style="width:100px;height:100px">
                                </div>
                                <div class="col-md-8">
                                    <h4><?= $info['title']; ?></h4>
                                    <h5 style="margin-top: 5%"><?= $info['msg']; ?></h5>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>

            <div class="login-main">
                <div>
                    <!-- <h1>Login</h1> -->
                    <div id="login-row">
                        <div style="margin: auto">
                            <img src="<?php echo base_url() ?>assets/images/logo-homes-updated.png" style="margin-top: 30px" width="200" height="80">
                            <!-- <h1 style="text-align: center; color: #36ae97">homes</h1> -->
                        </div>
                    </div>
                </div>
                <div class="login-block">
                    <form action="<?php echo base_url('welcome/login'); ?>" method="post">
                        <input type="text" name="username" placeholder="Username" required="">
                        <input type="password" name="password" class="lock" placeholder="Password">
                        <!-- <div class="forgot-top-grids">
                            <div class="forgot-grid">
                                <ul>
                                    <li>
                                        <input type="checkbox" id="brand1" value="">
                                        <label for="brand1"><span></span>Remember me</label>
                                    </li>
                                </ul>
                            </div>
                            <div class="forgot">
                                <a href="#">Forgot password?</a>
                            </div>
                            <div class="clearfix"> </div>
                        </div> -->
                        <input type="submit" name="Sign In" value="Login">
                        <!-- <h3>Not a member?<a href="signup.html"> Sign up now</a></h3> -->
                        <!-- <h2>or login with</h2> -->
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--inner block end here-->

    <!--scrolling js-->
    <script src="js/jquery.nicescroll.js"></script>
    <script src="js/scripts.js"></script>
    <!--//scrolling js-->
    <script src="js/bootstrap.js"> </script>
    <!-- mother grid end here-->
</body>