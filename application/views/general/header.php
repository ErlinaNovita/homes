<body>
    <div class="page-container">
        <div class="left-content">
            <div class="mother-grid-inner">
                <!--header start here-->
                <div class="header-main">
                    <div class="header-left">
                        <div class="logo-name">
                            <a href="#">
                                <!-- <h1>HOMES</h1> -->
                                <img src="<?php echo base_url() ?>assets/images/logo-homes-updated.png" style="max-width:130px;">
                            </a>
                        </div>
                        <!--search-box-->
                        <div class="search-box">
                            <form>
                                <input type="text" placeholder="Search..." required="">
                                <input type="submit" value="">
                            </form>
                        </div>
                        <!--//end-search-box-->
                        <div class="clearfix"> </div>
                    </div>
                    <div class="header-right">
                        <!--notification menu end -->
                        <div class="profile_details">
                            <ul>
                                <li class="dropdown profile_details_drop">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <div class="profile_img">
                                            <span class="prfil-img"><img src="images/p1.png" alt=""> </span>
                                            <div class="user-name">
                                                <p>Hai, <?php echo $this->session->userdata("nama"); ?></p>
                                                <span><?php
                                                        if ($this->session->userdata("role") == 1) {
                                                            echo 'Admin';
                                                        } else if ($this->session->userdata("role") == 2) {
                                                            echo 'Customer';
                                                        } else if ($this->session->userdata("role") == 3) {
                                                            echo 'Penyedia Jasa';
                                                        } ?></span>
                                            </div>
                                            <i class="fa fa-angle-down lnr"></i>
                                            <i class="fa fa-angle-up lnr"></i>
                                            <div class="clearfix"></div>
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu drp-mnu">
                                        <!-- <li> <a href="#"><i class="fa fa-cog"></i> Settings</a> </li> -->
                                        <!-- <li> <a href="#"><i class="fa fa-user"></i> Profile</a> </li> -->
                                        <li> <a href="<?php echo base_url('welcome/logout'); ?>"><i class="fa fa-sign-out"></i> Logout</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="profile_details">
                            <!--notifications of menu start -->
                            <ul class="nofitications-dropdown">
                                <li class="dropdown head-dpdn">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge success">3</span></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="notification_header">
                                                <h3>Notifikasi</h3>
                                            </div>
                                        </li>
                                        <li><a href="#">
                                                <div class="notification_desc">
                                                    <p>Transaksi Baru</p>
                                                    <p><span>5 menit yang lalu</span></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </a>
                                        </li>
                                        <li><a href="#">
                                                <div class="notification_desc">
                                                    <p>Transaksi Baru</p>
                                                    <p><span>1 jam yang lalu</span></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </a>
                                        </li>
                                        <li><a href="#">
                                                <div class="notification_desc">
                                                    <p>Transaksi Baru</p>
                                                    <p><span>5 jam yang lalu</span></p>
                                                </div>
                                                <div class="clearfix"></div>
                                            </a>
                                        </li>
                                        <li>
                                            <div class="notification_bottom">
                                                <a href="#">Lihat Semua</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <script>
                    $(document).ready(function() {
                        var navoffeset = $(".header-main").offset().top;
                        $(window).scroll(function() {
                            var scrollpos = $(window).scrollTop();
                            if (scrollpos >= navoffeset) {
                                $(".header-main").addClass("fixed");
                            } else {
                                $(".header-main").removeClass("fixed");
                            }
                        });

                    });
                </script>
                <div class="inner-block">